import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { GridPage } from '../grid/grid';
// import { PostService } from '../../app/post.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  // providers: [PostService]
})
export class HomePage {

  mostrarTexto: boolean;
  hobbies: string[];
  name: string;
  constructor(public navCtrl: NavController, public alerta: AlertController /*, private postService:PostService*/) {
    this.mostrarTexto = false;
    this.hobbies = ['run', 'sleep'];
    this.name = "Sara";
    // this.postService.getPosts().subscribe(post=>{
    //   console.log(post);
    // });
  }

  otraPagina(){
  	this.navCtrl.push(GridPage);
  }

  mostrarAlerta(){
  	let miAlerta = this.alerta.create({
  		title: 'Titulo de la alerta',
  		message: 'esto es una prueba de la alerta ',
  		buttons: ['Entendido']
  	});
  	miAlerta.present();
  }

  mostrarAlerta2(){
    let miAlerta2 = this.alerta.create({
      title: 'Login',
      message: 'esto es una prueba de la alerta con un input ',
      inputs: [{
        name: 'Nombre',
        placeholder: 'Nombre'
      }],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Guardar',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    miAlerta2.present();
  }

  mostrarOcultar(){
    this.mostrarTexto = !this.mostrarTexto;
  }

  textoInput(texto){
    this.hobbies.push(texto.value);
    texto.value = '';
    return false;
  }
}
