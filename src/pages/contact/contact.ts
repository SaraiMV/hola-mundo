import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MasinfoPage } from '../masinfo/masinfo';
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

	lista: Array<any> = [
		{
		 titulo: "Perro",
		 color: "#444"
		},
		{
		 titulo: "Gato",
		 color: "#999"
		}
	]
	
	miLista = [];

  constructor(public navCtrl: NavController) {
  		this.miLista = [
  			{
  				'title': 'Angular',
  				'icon': 'angular',
  				'description': 'hey soy angular',
  				'color': 'green'	
  			},
  			{
  				'title': 'CSS3',
  				'icon': 'css3',
  				'description': 'hey soy css3',
  				'color': 'red'
  			},
  			{
  				'title': 'HTML',
  				'icon': 'html5',
  				'description': 'hey soy html',
  				'color': 'blue'
  			}
  		]
  }

  abrirDetallesPagina(item){
  	this.navCtrl.push(MasinfoPage, {item: item})
  }

}
